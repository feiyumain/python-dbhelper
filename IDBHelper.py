#!/usr/bin/python
# -*- coding: utf-8 -*-

""" 
-------------------------------------------------
@version    : v1.0 
@author     : fangzheng
@contact    : zfang@hillinsight.com
@software   : PyCharm 
@filename   : IDBHelper.py
@create time: 2019/3/26 18:00 
@describe   : 数据库帮助类：抽象基类
@use example: python DBHelperI.py [param1 param2]
-------------------------------------------------
"""

from abc import ABCMeta, abstractmethod


# 通过ABCMeta模块可以构建一个抽象基类, 抽象基类不能够实例化 例如：dbhelper = DBHelper()
# 并且@abstractmethod标注的方法必须被子类重写

class DBHelper(metaclass=ABCMeta):
    @abstractmethod
    def init(self,dbconfig):
        """
        初始化数据库连接
        :param dbconfig: 数据库连接对象
        :return: True/False
        """
        pass

    @abstractmethod
    def table_is_exist(self,tablename):
        """
        判断表是否存在
        :param tablename:
        :return:
        """
        pass

    @abstractmethod
    def select(self,sql,param=None,size=None):
        """
        查询数据
        :param sql:
        :param param:
        :param size: 期望返回的数据条数 为空则返回全部数据
        :return:
        """
        pass

    @abstractmethod
    def execute(self,sql,param=None):
        """
        执行DML语句：INSERT、UPDATE、DELETE
        :param sql:
        :param param:
        :return:
        """
        pass

    @abstractmethod
    def get_conn(self):
        """
        获取一个连接
        :return:
        """
        pass

    @abstractmethod
    def close_conn(self):
        """
        关闭连接
        :return:
        """
        pass
